package com.yehq.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBConn
{
    public static Connection conn = null;
    public ResultSet rs = null;
    public Statement st = null;

    private static String driver = "oracle.jdbc.driver.OracleDriver";
    private static String url = "jdbc:oracle:thin:@134.129.126.133:1522:crm2dev3";
    private static String username = "crmv2";
    private static String password = "crmv2123";
    private static DBConn dbConn = null;

    public static DBConn getDBConn()
    {
        if (dbConn == null) {
            dbConn = new DBConn();
        }
        return dbConn;
    }

    public Connection getConnection()
    {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static void close(Statement st, ResultSet rs, Connection conn)
    {
        try {
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (conn != null)
                conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(Statement st)
    {
        try {
            if (st != null)
                st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(ResultSet rs)
    {
        try {
            if (rs != null)
                rs.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(Connection conn)
    {
        try {
            if (conn != null)
                conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
