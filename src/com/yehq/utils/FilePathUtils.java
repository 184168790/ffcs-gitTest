//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yehq.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FilePathUtils {
    public FilePathUtils() {
    }

    public static boolean copyFile(String srcFileName, String destFileName) {
        return copyFile(srcFileName, destFileName, false);
    }

    public static boolean copyFile(String srcFileName, String destFileName, boolean overlay) {
        File srcFile = new File(srcFileName);
        if (!srcFile.exists()) {
            System.out.println("复制文件失败：原文件" + srcFileName + "不存在！");
            return false;
        } else if (!srcFile.isFile()) {
            System.out.println("复制文件失败：" + srcFileName + "不是一个文件！");
            return false;
        } else {
            File destFile = new File(destFileName);
            boolean success;
            if (destFile.exists()) {
                if (!overlay) {
                    System.out.println("复制文件失败：目标文件" + destFileName + "已存在！");
                    return false;
                }

                System.out.println("目标文件已存在，准备删除它！");
                success = deleteDir(destFile);
                success = false;
                if (false) {
                    System.out.println("复制文件失败：删除目标文件" + destFileName + "失败！");
                    return false;
                }
            } else if (!destFile.getParentFile().exists()) {
                System.out.println("目标文件所在的目录不存在，准备创建它！");
                if (!destFile.getParentFile().mkdirs()) {
                    System.out.println("复制文件失败：创建目标文件所在的目录失败！");
                    return false;
                }
            }

            success = false;
            InputStream in = null;
            FileOutputStream out = null;

            try {
                in = new FileInputStream(srcFile);
                out = new FileOutputStream(destFile);
                byte[] buffer = new byte[1024];

                int byteread;
                while((byteread = in.read(buffer)) != -1) {
                    out.write(buffer, 0, byteread);
                }

                System.out.println("复制单个文件" + srcFileName + "至" + destFileName + "成功！");
                return true;
            } catch (Exception var21) {
                System.out.println("复制文件失败：" + var21.getMessage());
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException var20) {
                        var20.printStackTrace();
                    }
                }

                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException var19) {
                        var19.printStackTrace();
                    }
                }

            }

            return false;
        }
    }

    public static boolean deleteDir(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                File[] var5 = files;
                int var4 = files.length;

                for(int var3 = 0; var3 < var4; ++var3) {
                    File subFile = var5[var3];
                    if (subFile.isDirectory()) {
                        deleteDir(subFile);
                    } else {
                        subFile.delete();
                    }
                }
            }

            file.delete();
        }

        return true;
    }
}
