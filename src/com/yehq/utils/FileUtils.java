package com.yehq.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class FileUtils
{
    private static FileUtils fileUtils = null;
    private static Properties prop = new Properties();

    public FileUtils()
    {
        InputStream inStream = getClass().getResourceAsStream(
                "/default.properties");
        try {
            prop.load(inStream);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static FileUtils getInstance() {
        if (fileUtils == null) {
            fileUtils = new FileUtils();
        }
        return fileUtils;
    }

    public static String getProperty(String key)
    {
        getInstance();
        return (String)prop.get(key);
    }

    public static ResultSet toAddFile(String fileNames) {
        String[] files = fileNames.split(",");
        DBConn dbConn = DBConn.getDBConn();
        Connection conn = null;
        ResultSet rs = null;
        conn = dbConn.getConnection();
        if (conn != null) {
            System.out.println("success!!");
        }
        StringBuffer sql = new StringBuffer();
        sql.append("select * from v_crm2_zlbg@lk_crm2_cq a  where a.cq单号  ");
        List params = new ArrayList();
        if ((files != null) && (files.length > 0)) {
            if (files.length == 1) {
                sql.append(" = ?");
                params.add(files[0]);
            } else {
                sql.append(" in ( ");
                for (int i = 0; i < files.length; i++) {
                    if (i == 0)
                        sql.append("?");
                    else {
                        sql.append(",?");
                    }
                    params.add(files[i]);
                }
                sql.append(") ");
            }
        }

        try
        {
            PreparedStatement ps = conn.prepareStatement(sql.toString());
            for (int i = 0; i < params.size(); i++) {
                String str = (String)params.get(i);
                ps.setString(i + 1, str);
            }
            rs = ps.executeQuery();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            DBConn.close(conn);
        }

        return rs;
    }
}
