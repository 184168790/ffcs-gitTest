package com.yehq.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static final String getDateByDateFormat(String dateFormat, Date aDate) {
        SimpleDateFormat df = null;
        String returnValue = "";

        if (aDate != null) {
            df = new SimpleDateFormat(dateFormat);
            returnValue = df.format(aDate);
        }
        return (returnValue);
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getDateByDateFormat("yyyy-MM-dd", new Date()));
    }
}
