//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yehq.ui;

import com.yehq.utils.DBConn;
import com.yehq.utils.FilePathUtils;
import com.yehq.utils.FileUtils;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Date;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class IndexFrame extends JFrame implements ActionListener {
    private JLabel sourceJLabel = new JLabel("源  文 件 夹");
    private JTextField sourceJText = new JTextField(46);
    private JLabel targetJLabel = new JLabel("目标文件夹");
    private JTextField targetJText = new JTextField(46);
    private JLabel dealJLabel = new JLabel("涉  及 文 件");
    private JLabel fileNameJLabel = new JLabel(" 文    件    夹");
    private JTextField fileNameJText = new JTextField(20);
    private JCheckBox checkbox = new JCheckBox("增量文件");
    private JTextField cqsJText = new JTextField(20);
    private JTextArea dealJText = new JTextArea(10, 50);
    private JLabel resultJLabel = new JLabel("结            果");
    private JTextArea resultJText = new JTextArea(10, 50);
    private JButton submitBtn = new JButton("确定");
    private JButton cancelBtn = new JButton("取消");
    private JButton selFile = new JButton("...");
    private JButton souFile = new JButton("...");
    private static ByteArrayOutputStream baoStream = new ByteArrayOutputStream(1024);
    private static PrintStream cacheStream;
    JFileChooser jfc = new JFileChooser();

    static {
        cacheStream = new PrintStream(baoStream);
    }

    public IndexFrame() {
        this.setDefaultCloseOperation(3);
        this.setResizable(false);
        this.setTitle("文件复制工具-生产代码-chensl修改3333333");
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.setLayout(new BoxLayout(panel, 1));
        JPanel panel1 = new JPanel();
        panel1.setLayout(new FlowLayout(0));
        panel1.add(this.sourceJLabel);
        panel1.add(this.sourceJText);
        this.sourceJText.setText("D:\\FFCS\\代码\\idea代码\\HNCRM3.0\\受控库\\CODE（受控代码）\\生产代码");
        panel1.add(this.selFile);
        panel.add(panel1);
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(0));
        panel2.add(this.targetJLabel);
        panel2.add(this.targetJText);
        this.targetJText.setText("D:\\FFCS\\增量文件-在主分支上随便修改点内容");
        panel2.add(this.souFile);
        panel.add(panel2);
        JPanel panel21 = new JPanel();
        panel21.setLayout(new FlowLayout(0));
        panel21.add(this.fileNameJLabel);
        fileNameJText.setText(DateUtil.getDateByDateFormat("yyyy-MM-dd", new Date()));
        panel21.add(this.fileNameJText);
        panel21.add(this.checkbox);
        this.checkbox.setSelected(false);
        panel21.add(this.cqsJText);
        this.cqsJText.setVisible(true);
        panel.add(panel21);
        JPanel panel3 = new JPanel();
        panel3.setLayout(new FlowLayout(0));
        panel3.add(this.dealJLabel);
        this.dealJText.setTabSize(10);
        panel3.add(new JScrollPane(this.dealJText));
        panel.add(panel3);
        JPanel panel4 = new JPanel();
        panel4.setLayout(new FlowLayout(0));
        panel4.add(this.resultJLabel);
        this.resultJText.setTabSize(10);
        panel4.add(new JScrollPane(this.resultJText));
        panel.add(panel4);
        JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout());
        panel5.add(this.submitBtn);
        panel5.add(this.cancelBtn);
        panel.add(panel5);
        this.checkbox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (IndexFrame.this.checkbox.isSelected()) {
                    IndexFrame.this.cqsJText.setVisible(true);
                } else {
                    IndexFrame.this.cqsJText.setVisible(false);
                }

            }
        });
        this.submitBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IndexFrame.this.resultJText.setText("");
                if (IndexFrame.this.dealJText.getText() != null && !"".equals(IndexFrame.this.dealJText.getText().trim())) {
                    if (IndexFrame.this.sourceJText.getText() != null && !"".equals(IndexFrame.this.sourceJText.getText().trim())) {
                        if (IndexFrame.this.targetJText.getText() != null && !"".equals(IndexFrame.this.targetJText.getText().trim())) {
                            if (IndexFrame.this.fileNameJText.getText() != null && !"".equals(IndexFrame.this.fileNameJText.getText().trim())) {
                                if (IndexFrame.this.checkbox.isSelected()) {
                                    DBConn dbConn = DBConn.getDBConn();
                                    Connection conn = null;
                                    conn = dbConn.getConnection();
                                    if (conn == null) {
                                        JOptionPane.showMessageDialog(IndexFrame.this.sourceJText, "无法连接数据库，无法导出增量明细!");
                                        IndexFrame.this.checkbox.setSelected(false);
                                        return;
                                    }
                                }

                                String source = IndexFrame.this.sourceJText.getText().trim();
                                String target = IndexFrame.this.targetJText.getText().trim();
                                String fileName = IndexFrame.this.fileNameJText.getText().trim();
                                String[] files = IndexFrame.this.dealJText.getText().split("\n");
                                String[] var9 = files;
                                int var8 = files.length;

                                for(int var7 = 0; var7 < var8; ++var7) {
                                    String path = var9[var7];
                                    if (path != null || !"".equals(path)) {
                                        if (!path.substring(0, 1).equals("/")) {
                                            path = "/" + path;
                                        }

                                        String oldPath = source + "/" + path;
                                        String newPath = target + "/" + fileName + path;
                                        FilePathUtils.copyFile(oldPath.replace("/", "\\\\"), newPath.replace("/", "\\\\"));
                                    }
                                }

                                IndexFrame.this.resultJText.setText(IndexFrame.baoStream.toString());
                                if (IndexFrame.this.checkbox.isSelected()) {
                                    try {
                                        IndexFrame.this.export(IndexFrame.this.cqsJText.getText(), target + "/" + fileName);
                                    } catch (Exception var14) {
                                        var14.printStackTrace();
                                    } finally {
                                        DBConn var18 = DBConn.getDBConn();
                                        DBConn.close(DBConn.conn);
                                    }
                                }

                                JOptionPane.showMessageDialog((Component)null, "文件复制成功!");
                            } else {
                                JOptionPane.showMessageDialog(IndexFrame.this.fileNameJText, "文件名不能为空!");
                                IndexFrame.this.fileNameJText.requestFocus();
                            }
                        } else {
                            JOptionPane.showMessageDialog(IndexFrame.this.sourceJText, "请选择目标文件!");
                            IndexFrame.this.sourceJText.requestFocus();
                        }
                    } else {
                        JOptionPane.showMessageDialog(IndexFrame.this.sourceJText, "请选择源文件!");
                        IndexFrame.this.sourceJText.requestFocus();
                    }
                } else {
                    JOptionPane.showMessageDialog(IndexFrame.this.dealJText, "涉及文档不能为空!");
                    IndexFrame.this.dealJText.requestFocus();
                }
            }
        });
        this.cancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        this.selFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = sourceJText.getText();
                File file = new File(text);
                IndexFrame.this.jfc.setSelectedFile(file);
                int state = IndexFrame.this.jfc.showOpenDialog((Component)null);
                if (state != 1) {
                    File f = IndexFrame.this.jfc.getSelectedFile();
                    IndexFrame.this.sourceJText.setText(f.getAbsolutePath());
                }
            }
        });
        this.souFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int state = IndexFrame.this.jfc.showOpenDialog((Component)null);
                if (state != 1) {
                    File f = IndexFrame.this.jfc.getSelectedFile();
                    IndexFrame.this.targetJText.setText(f.getAbsolutePath());
                }
            }
        });
        this.jfc.setFileSelectionMode(1);
        this.setContentPane(panel);
        this.setLocation(400, 300);
        this.setSize(700, 600);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
    }

    public static void main(String[] arg) {
        System.setOut(cacheStream);
        new IndexFrame();
    }

    private void export(String fileNames, String target) throws Exception {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Sheet1");
        HSSFRow row = sheet.createRow(0);
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment((short)2);
        HSSFFont font = wb.createFont();
        font.setBoldweight((short)700);
        HSSFCellStyle style1 = wb.createCellStyle();
        style1.setAlignment((short)2);
        style1.setFont(font);
        ResultSet rs = FileUtils.toAddFile(fileNames);
        if (rs != null) {
            ResultSetMetaData rsmd = rs.getMetaData();

            int index;
            for(index = 0; index < rsmd.getColumnCount(); ++index) {
                HSSFCell cell = row.createCell(index);
                cell.setCellValue(rsmd.getColumnName(index + 1));
                cell.setCellType(1);
                cell.setCellStyle(style1);
            }

            for(index = 1; rs.next(); ++index) {
                HSSFRow row1 = sheet.createRow(index);

                for(int i = 0; i < rsmd.getColumnCount(); ++i) {
                    row1.createCell(i).setCellValue(rs.getString(i + 1));
                }
            }
        }

        FileOutputStream fout = new FileOutputStream(target + "/增量明细.xls");
        wb.write(fout);
        fout.close();
    }
}
